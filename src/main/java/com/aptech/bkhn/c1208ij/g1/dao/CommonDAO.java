/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptech.bkhn.c1208ij.g1.dao;

import com.aptech.bkhn.c1208ij.g1.entity.User;
import com.aptech.bkhn.c1208ij.g1.listener.HibernateListener;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author ThucNV
 */
public class CommonDAO {

    private User feild;
    SessionFactory sf;

    public CommonDAO() {
        sf = (SessionFactory) ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME);
    }

    public CommonDAO(User feild) {
        this.feild = feild;
    }

    public SessionFactory getSf() {
        return sf;
    }

    public void setSf(SessionFactory sf) {
        this.sf = sf;
    }

    

}
