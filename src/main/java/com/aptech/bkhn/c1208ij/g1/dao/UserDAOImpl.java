/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptech.bkhn.c1208ij.g1.dao;

import com.aptech.bkhn.c1208ij.g1.entity.User;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ThucNV
 */
public class UserDAOImpl extends CommonDAO {
    // private User user;

//    public UserDAOImpl(Class<User> entityClass) {
//        super(entityClass);
//    }
    public List<User> getAll() {
        List lst;

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from User");
        query.setFirstResult(0);
        query.setMaxResults(10);
        lst = query.list();
        
        transaction.commit();
        session.close();
        
        return lst;
    }

}
