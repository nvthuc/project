/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptech.bkhn.c1208ij.g1.action;

import com.aptech.bkhn.c1208ij.g1.dao.UserDAOImpl;
import com.aptech.bkhn.c1208ij.g1.entity.User;
import com.aptech.bkhn.c1208ij.g1.listener.HibernateListener;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import org.apache.struts2.ServletActionContext;
import org.hibernate.SessionFactory;

/**
 *
 * @author ThucNV
 */
public class UserAction extends ActionSupport {

    private User user;
    private List<User> users;
    private UserDAOImpl userDAOImpl = new UserDAOImpl();

    public UserDAOImpl getUserDAOImpl() {
        return userDAOImpl;
    }

    public void setUserDAOImpl(UserDAOImpl userDAOImpl) {
        this.userDAOImpl = userDAOImpl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public UserAction() {
    }

    public String execute() throws Exception {
        users = userDAOImpl.getAll();
        SessionFactory sf = (SessionFactory) ServletActionContext.getServletContext().getAttribute(HibernateListener.KEY_NAME);
        mess = sf != null;
        return SUCCESS;
    }

    private boolean mess;

    public boolean getMess() {
        return mess;
    }

    public void setMess(boolean mess) {
        this.mess = mess;
    }

}
