<%-- 
    Document   : baseLayout
    Created on : Nov 15, 2014, 12:36:00 PM
    Author     : ThucNV
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><t:insertAttribute name="title" ignore="true"/></title>
        <!-- stylesheets -->
        <t:importAttribute name="stylesheets"/>
        <c:forEach var="css" items="${stylesheets}">
            <link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
        </c:forEach>
        <!-- end stylesheets -->
    </head>
    <body>
        <hr>
        <t:insertAttribute name="header"/>
        <hr>
        <t:insertAttribute name="body"/>
        <hr>
        <t:insertAttribute name="footer"/>
        <hr>

        <!-- scripts -->
        <t:importAttribute name="javascripts"/>
        <c:forEach var="script" items="${javascripts}">
            <script src="<c:url value="${script}"/>"></script>
        </c:forEach>
            
        <!-- end scripts -->
    </body>
</html>
